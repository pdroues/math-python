import pandas as pd
import numpy as np

# Importez le fichier CSV en utilisant Pandas
df = pd.read_csv('toy_complet.csv', sep=" ")

# deux vecteurs
A = df.iloc[:, 1].to_numpy()
B = df.iloc[:, 2].to_numpy()

# Calcul du produit scalaire entre A et B
dot_product = np.dot(A, B)

# Calcul des normes euclidiennes de A et B
norm_A = np.linalg.norm(A)
norm_B = np.linalg.norm(B)

# Calcul de la similarité cosinus
cosine_similarity = dot_product / (norm_A * norm_B)

print("Similarité cosinus entre A et B :", cosine_similarity)