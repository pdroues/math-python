import pandas as pd
import numpy as np

def similarite_pearson(colonne_X, colonne_Y):
    # Calcul des moyennes de X et Y
    mean_X = colonne_X.mean()
    mean_Y = colonne_Y.mean()
    # Calcul du numérateur et des dénominateurs
    numerator = sum((x - mean_X) * (y - mean_Y) for x, y in zip(colonne_X, colonne_Y))
    denominator_X = np.sqrt(sum((x - mean_X) ** 2 for x in colonne_X))
    denominator_Y = np.sqrt(sum((y - mean_Y) ** 2 for y in colonne_Y))
    # Calcul de la similarité de Pearson
    pearson_similarity = numerator / (denominator_X * denominator_Y)
    return pearson_similarity

def similarite_cosinus(colonne_X, colonne_Y):
    # Calcul du produit scalaire
    dot_product = np.dot(colonne_X, colonne_Y)
    # Calcul des normes
    norm_X = np.linalg.norm(colonne_X)
    norm_Y = np.linalg.norm(colonne_Y)
    # Calcul de la similarité cosinus
    cosine_similarity = dot_product / (norm_X * norm_Y)
    return cosine_similarity

def user_based_recommendation(user_id, matrice_similarite, data):
    user_ratings = data.iloc[user_id, :]
    weighted_sum = np.zeros(data.shape[1])
    sum_of_weights = np.zeros(data.shape[1])

    for i in range(data.shape[0]):
        if i != user_id:
            similarity = matrice_similarite[user_id, i]
            for j in range(data.shape[1]):
                if np.isnan(user_ratings.iloc[j]) and not np.isnan(data.iloc[i, j]):
                    weighted_sum[j] += similarity * data.iloc[i, j]
                    sum_of_weights[j] += abs(similarity)

    predicted_ratings = weighted_sum / (sum_of_weights + 1e-10)  # Avoid division by zero
    return predicted_ratings

def item_based_recommendation(user_id, matrice_similarite, data):
    user_ratings = data.iloc[user_id, :]
    predicted_ratings = np.zeros(data.shape[1])

    for i in range(data.shape[1]):
        if np.isnan(user_ratings[i]):
            weighted_sum = 0
            sum_of_weights = 0

            for j in range(data.shape[1]):
                if i != j and not np.isnan(user_ratings.iloc[j]) and not np.isnan(data.iloc[user_id, j]):
                    similarity = matrice_similarite[i, j]
                    weighted_sum += similarity * data.iloc[user_id, j]
                    sum_of_weights += abs(similarity)

            if sum_of_weights > 0:
                predicted_ratings[i] = weighted_sum / sum_of_weights

    return predicted_ratings

# Charger le jeu de données
df = pd.read_csv('toy_complet.csv', sep=' ')
matrice_similarite = np.zeros((100, 100))

for i in range(100):
    for j in range(100):
        matrice_similarite[i,j] = np.round(similarite_pearson(df.iloc[:, i].to_numpy(), df.iloc[:, j].to_numpy()), 3)

print("matrice similarité")
print(matrice_similarite)

# Exemple d'utilisation du filtre basé sur l'utilisateur pour recommander des éléments à l'utilisateur 0
user_id = 0
user_based_recommendations = user_based_recommendation(user_id, matrice_similarite, df)
print("User-Based Recommendations for User {}: {}".format(user_id, user_based_recommendations))

# Exemple d'utilisation du filtre basé sur l'item pour recommander des éléments à l'utilisateur 0
item_based_recommendations = item_based_recommendation(user_id, matrice_similarite, df)
print("Item-Based Recommendations for User {}: {}".format(user_id, item_based_recommendations))