# Code pour avoir une matrice de similarités

import pandas as pd
import numpy as np

################################### FONCTIONS

def similarite_pearson(colonne_X, colonne_Y):
    # Calcul des moyennes de X et Y
    mean_X = colonne_X.mean()
    mean_Y = colonne_Y.mean()
    # Calcul du numérateur et des dénominateurs
    numerator = sum((x - mean_X) * (y - mean_Y) for x, y in zip(colonne_X, colonne_Y))
    denominator_X = np.sqrt(sum((x - mean_X) ** 2 for x in colonne_X))
    denominator_Y = np.sqrt(sum((y - mean_Y) ** 2 for y in colonne_Y))
    # Calcul de la similarité de Pearson
    pearson_similarity = numerator / (denominator_X * denominator_Y)
    return pearson_similarity

def matrice_similarite_pearson(data):
    nombre_lignes, nombre_colonnes = data.shape
    matrice_pearson = np.zeros((nombre_lignes, nombre_colonnes))
    for i in range(nombre_lignes):
        for j in range(nombre_colonnes):
            matrice_pearson[i,j] = np.round(similarite_pearson(data.iloc[:, i].to_numpy(), data.iloc[:, j].to_numpy()), 3)
    return matrice_pearson

def similarite_cosinus(colonne_X, colonne_Y):
    # Calcul du produit scalaire entre A et B
    dot_product = np.dot(colonne_X, colonne_Y)
    # Calcul des normes euclidiennes de A et B
    norm_X = np.linalg.norm(colonne_X)
    norm_Y = np.linalg.norm(colonne_Y)
    # Calcul de la similarité cosinus
    return dot_product / (norm_X * norm_Y)

def matrice_similarite_cosinus(data):
    nombre_lignes, nombre_colonnes = data.shape
    matrice_cosinus = np.zeros((nombre_lignes, nombre_colonnes))
    for i in range(nombre_lignes):
        for j in range(nombre_colonnes):
            matrice_cosinus[i,j] = np.round(similarite_cosinus(data.iloc[:, i].to_numpy(), data.iloc[:, j].to_numpy()), 3)
    return matrice_cosinus

def cosine_similarity_ignore_missing(A, B):
    common_ratings = np.logical_and(~np.isnan(A), ~np.isnan(B))
    dot_product = np.sum(A[common_ratings] * B[common_ratings])
    norm_A = np.sqrt(np.sum(A[common_ratings] ** 2))
    norm_B = np.sqrt(np.sum(B[common_ratings] ** 2))
    similarity_cosine = dot_product / (norm_A * norm_B)
    return similarity_cosine
            
def matrice_cosine_similarity_ignore_missing(data):
    nombre_lignes, nombre_colonnes = data.shape
    matrice_cosinus_missing = np.zeros((nombre_lignes, nombre_colonnes))
    for i in range(nombre_lignes):
        for j in range(nombre_colonnes):
            matrice_cosinus_missing[i,j] = np.round(cosine_similarity_ignore_missing(dfi.iloc[:, i].to_numpy(), dfi.iloc[:, j].to_numpy()), 3)
    
################################### ACTIONS

df = pd.read_csv('toy_complet.csv', sep=" ", header=None)
dfi = pd.read_csv('toy_incomplet.csv', sep=" ", header=None)

def cosine_similarity_ignore_missing(A, B):
    common_ratings = np.logical_and(~np.isnan(A), ~np.isnan(B))
    dot_product = np.sum(A[common_ratings] * B[common_ratings])
    norm_A = np.sqrt(np.sum(A[common_ratings] ** 2))
    norm_B = np.sqrt(np.sum(B[common_ratings] ** 2))
    similarity_cosine = dot_product / (norm_A * norm_B)
    return similarity_cosine

def pedict_note(matrice, similarite, val):
    for i in range(0, 99):
        for j in range(0, 99):
            if (np.isnan(similarite[i,j])):
                note_of_similar = np.array()
                for k in range(0, 99):
                    if (similarite[i,k] > val):
                        note_of_similar = np.append(note_of_similar, matrice[k,f])
                similarite[i,j] = np.mean(note_of_similar)
    return similarite
    

matrice_cosinus_missing = np.zeros((nombre_lignes, nombre_colonnes))

for i in range(0, 99):
    for j in range(0, 99):
        

print("matrice cosinus missing")
print(matrice_cosinus_missing)


predicted_note = pedict_note(df, matrice_cosinus_missing, 0.2) 
print("prédiction")
print(predicted_note)
