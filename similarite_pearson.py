import pandas as pd
import numpy as np
from scipy.stats import pearsonr

# Importez le fichier CSV en utilisant Pandas
df = pd.read_csv('toy_complet.csv', sep=" ")

# Assurez-vous que les indices 1 et 2 sont valides en fonction du nombre de colonnes dans le DataFrame
if 1 < len(df.columns) and 2 < len(df.columns):
    colonne_X = df.iloc[:, 1].to_numpy()
    colonne_Y = df.iloc[:, 2].to_numpy()

    # Calcul des moyennes de X et Y
    mean_X = colonne_X.mean()
    mean_Y = colonne_Y.mean()

    # Calcul du numérateur et des dénominateurs
    numerator = sum((x - mean_X) * (y - mean_Y) for x, y in zip(colonne_X, colonne_Y))
    denominator_X = np.sqrt(sum((x - mean_X) ** 2 for x in colonne_X))
    denominator_Y = np.sqrt(sum((y - mean_Y) ** 2 for y in colonne_Y))

    # Calcul de la similarité de Pearson
    pearson_similarity = numerator / (denominator_X * denominator_Y)

    print("Similarité de Pearson entre les colonnes X et Y :", pearson_similarity)
else:
    print("Les indices spécifiés sont en dehors des limites du DataFrame.")

# Exemple de deux ensembles de données
X = df.iloc[:, 1].to_numpy()
Y = df.iloc[:, 2].to_numpy()

# Calcul de la similarité de Pearson
corr_coefficient, p_value = pearsonr(X, Y)

print("Coefficient de corrélation de Pearson :", corr_coefficient)