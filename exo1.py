# Code pour avoir une matrice de similarités

import pandas as pd
import numpy as np

np.set_printoptions(threshold=np.inf)

################################### FONCTIONS

def similarite_pearson(colonne_X, colonne_Y):
    # Calcul des moyennes de X et Y
    mean_X = colonne_X.mean()
    mean_Y = colonne_Y.mean()
    # Calcul du numérateur et des dénominateurs
    numerator = sum((x - mean_X) * (y - mean_Y) for x, y in zip(colonne_X, colonne_Y))
    denominator_X = np.sqrt(sum((x - mean_X) ** 2 for x in colonne_X))
    denominator_Y = np.sqrt(sum((y - mean_Y) ** 2 for y in colonne_Y))
    # Calcul de la similarité de Pearson
    pearson_similarity = numerator / (denominator_X * denominator_Y)
    return pearson_similarity

def matrice_similarite_pearson(data):
    nombre_lignes, nombre_colonnes = data.shape
    matrice_pearson = np.zeros((nombre_lignes, nombre_colonnes))
    for i in range(nombre_lignes):
        for j in range(nombre_colonnes):
            matrice_pearson[i,j] = np.round(similarite_pearson(data.iloc[:, i].to_numpy(), data.iloc[:, j].to_numpy()), 3)
    return matrice_pearson

def similarite_cosinus(colonne_X, colonne_Y):
    # Calcul du produit scalaire entre A et B
    dot_product = np.dot(colonne_X, colonne_Y)
    # Calcul des normes euclidiennes de A et B
    norm_X = np.linalg.norm(colonne_X)
    norm_Y = np.linalg.norm(colonne_Y)
    # Calcul de la similarité cosinus
    return dot_product / (norm_X * norm_Y)

def matrice_similarite_cosinus(data):
    nombre_lignes, nombre_colonnes = data.shape
    matrice_cosinus = np.zeros((nombre_lignes, nombre_colonnes))
    for i in range(nombre_lignes):
        for j in range(nombre_colonnes):
            matrice_cosinus[i,j] = np.round(similarite_cosinus(data.iloc[:, i].to_numpy(), data.iloc[:, j].to_numpy()), 3)
    return matrice_cosinus
    
################################### ACTIONS

df = pd.read_csv('toy_complet.csv', sep=" ")
subset_iloc = df.iloc[:10, :10]

print("matrice cosinus")
print(matrice_similarite_cosinus(subset_iloc))

print("matrice pearson")
print(matrice_similarite_pearson(subset_iloc))
